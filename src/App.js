import React from 'react';
import AccountStatus from './components/AccountStatus';
import Auth from './components/Auth';
import Balance from './components/Balance';
import Banking from './components/Banking';
import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
  return (
    <div className='form-group'>
        <Auth />
        <Balance />
        <Banking />
        <AccountStatus />
    </div>
  )
}

export default App