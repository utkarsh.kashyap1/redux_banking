import { TOGGLE_AUTH } from "../reducers/action.types";

export const toggle_auth = () => {
    return {type: TOGGLE_AUTH}
}