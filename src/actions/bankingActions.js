import {
  ACCOUNT_TYPE,
  COLLECT_INTEREST,
  DELETE_ACCOUNT,
  DEPOSIT,
  WITHDRAW,
} from "../reducers/action.types";

export const deposit = (amount) => {
  return { type: DEPOSIT, payload: parseInt(amount) };
};

export const withdraw = (amount) => {
  return { type: WITHDRAW, payload: parseInt(amount) };
};

export const interest = () => {
  return { type: COLLECT_INTEREST };
};

export const delete_account = () => {
  return { type: DELETE_ACCOUNT };
};

export const change_account_type = () => {
  return { type: ACCOUNT_TYPE };
};
