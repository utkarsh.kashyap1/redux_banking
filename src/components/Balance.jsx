import React from 'react';
import { useSelector } from "react-redux";

const Balance = () => {
  const balance = useSelector(state => state.banking.balance);
  return (
    <div className='form-control'>
        <h1>$ {balance}</h1>
    </div>
  )
}

export default Balance;