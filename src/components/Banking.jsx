import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { deposit, withdraw, interest, delete_account, change_account_type } from "../actions/bankingActions";

const Banking = () => {
  const [amount, setAmount] = useState(0);
  const dispatch = useDispatch();

  const handleDeposit = () => {
    dispatch(deposit(amount));
  } 

  const handleWithdraw = () => {
    dispatch(withdraw(amount));
  }

  const handleInterest = () => {
    dispatch(interest());
  }

  const handleDeleteAccount = () => {
    dispatch(delete_account());
  }

  const handleAccountType = () => {
    dispatch(change_account_type());
  }

  return (
    <div className="form-control">
      <input className='form-control mb-2' type="text" value={amount} onChange={(e) => setAmount(e.target.value)} />
      <button onClick={handleDeposit} className="btn btn-success">
        Deposit
      </button>
      <button onClick={handleWithdraw} className="btn btn-primary">
        Withdraw
      </button>
      <button onClick={handleInterest} className="btn btn-warning">
        Collect Interest
      </button>
      <button onClick={handleDeleteAccount} className="btn btn-danger">
        Delete Account
      </button>
      <button onClick={handleAccountType} className="btn btn-secondary">
        Change Account Type
      </button>
    </div>
  );
};

export default Banking;
