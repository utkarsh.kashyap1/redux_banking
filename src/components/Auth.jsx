import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toggle_auth } from '../actions/authActions';

const Auth = () => {
  const authState = useSelector(state => state.auth.isLoggedIn);
  const dispatch = useDispatch();

  const handleAuth = () => {
    dispatch(toggle_auth())
  }

  return (
    <div className='form-control'>
        <button className="btn btn-primary" onClick={handleAuth}>{authState ? 'Logout' : 'Login'}</button>
    </div>
  )
}

export default Auth;