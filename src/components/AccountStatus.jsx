import React from 'react'
import { useSelector } from 'react-redux'

const AccountStatus = () => {
  const account_type = useSelector(state => state.banking.account_type)
  return (
    <div className='form-control'>
        <h1>{account_type ? 'Savings' : 'Checking'} Account</h1>
    </div>
  )
}

export default AccountStatus