import { combineReducers } from "redux";
import { authReducers } from "./authReducers";
import { bankingReducers } from "./bankingReducers";

export const rootReducer = combineReducers({
    auth: authReducers,
    banking: bankingReducers,
});