import {
  DEPOSIT,
  WITHDRAW,
  COLLECT_INTEREST,
  DELETE_ACCOUNT,
  ACCOUNT_TYPE,
} from "./action.types.js";

export const initialState = {
  balance: 0,
  account_type: true,
};

export const bankingReducers = (state = initialState, action) => {
  switch (action.type) {
    case DEPOSIT:
      return {
        ...state,
        balance: state.balance + action.payload,
      };
    case WITHDRAW:
      return {
        ...state,
        balance: state.balance - action.payload,
      };
    case COLLECT_INTEREST:
      return {
        ...state,
        balance: state.balance * 1.03,
      };
    case DELETE_ACCOUNT:
      return initialState;
    case ACCOUNT_TYPE:
      console.log(state);
      return {
        ...state,
        account_type: !state.account_type,
      }
    default:
      return state;
  }
};
